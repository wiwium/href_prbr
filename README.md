## href_prbu


### Info
1) prbu module stands for parse and bucket.
2) It is use for parsing href and bucketing them form html pages using multiprocessing.

### Upper level Usecase

1) Install packages from requirements.txt
   ```bash
   pip3 install -r requirements.txt
   ```
2) Just copy the href_prbu folder into your project.
3) Then import in following format
   ```python
   from href_prbu import prbu
   ```
4) There are no classes here each function is meant to use independently\
 
5) First it is must to set config.py file

```python
PATHS = {
    "ZIP" : <zip file path>,
    "URLS_CSV_PATH" : <urls.csv path>,
    "Mongo_ip" : <ip>,
    "Mongo_port" : <port>,
    "mongo_database" : <database name>,
    "out_db" :  
  {'raw':<href_raw_collections_name>,
'clean':<href_clean_collection>,
'mail':<mail_collection_name>,'tel':<tel_collection_name>}
}
```

* *ZIP* : str
  * Html zip file path Containing downloaded htmls such that each .html in its own folder And that folders name is taken as 'id'
* *URLS_CSV_PATH* : str
  * Urls csv file containg 'uuid', 'name', 'website' or more columns as html page info (note 'uuid' is considered as id it must be unique and same as file name in zip file)
* *Mongo_ip* : str
  * Ip to connect to mongodb
* *Mongo_port* : int
  * mogodb port
* *mongo_database* : str
  * Database name
* *out_db.raw* : str
  * raw hrefs are stored in this collection name (just throw something there ;)
* *out_db.clean* : str
  * collection name in which will be created and stored href
* *out_db.mail*: str
  * collection name in which will be created and stored mails
* *tel* : str
  * collection name in which phone numbers are to be stored

5) Then use

   ```python
   prbu.run()
   ```

### Methods
1)
   ```python
   prbu.run() 
   ```
   Returns None\
   Use to start execution and must be called exactly once
-  Arguments
-  1) cap = None : int, use for testing

2)
   ```python
    prbu.find(bucket_list,output_db_name)
   ```
* Returns None\
* If you have called run() before your collections are created then you can call find() even in other time or other file.
* This function is used get bucketed href according to 'bucket_list'. **You can get example of keys by calling get_keys() funtion.**
* All the href related to it will be stored in 'output_db_name' i.e. second argument
* **Note : In config.py file 'config.bucket' dictionary contains some example with those run() method will internally call find() on those and keys will be collection names**
   
-  Arguments
-  1) bucket_list : list of str, list of keys eg(about,aboutus,linkedin etc)
-  2) output_db_name : str , name of collection in which href to be stored
-  3) db_clean_href = db[config.PATHS["out_db"]["clean"]], \<mongodb collection> \
all href collection default :- href_clean_collection of .run()

3)
   ```python
   prbu.get_keys()
   #eg
   for i in prbu.get_keys():
       print (i)
   ```
   Returns iterable keys and cout\
   **Use to get top keys i.e. (about,linkedin) that can be used in prbu.find() as bucket_list**
-  Arguments
-  1) db_clean_href= db[config.PATHS["out_db"]: all href collection default :- href_clean_collection of .run()
-  2) top= 10 :int, total keys to be extracted form top
-  3) tag = False : boolean, if True returns tag_key, False returns url_key
