PATHS = {
    "ZIP" : "/home/miti/ketan/data/discover_htmls/discoverhtmls.zip",
    "URLS_CSV_PATH" : "/home/miti/ketan/data/urls/discover_urls.csv",
    "Mongo_ip" : "localhost",
    "Mongo_port" : 27017,
    "mongo_database" : "discover",
    "out_db" : {'raw':'href_raw','clean':'href','mail':'href_mail','tel':'href_tel'}
}

bucket = {

    "contact_list_href" : ['contact', 'contactus', 'contacts', 'contactsales', 'contactsupport','about', 'aboutus'],

    "clients_list_href" :['clients','ourclients','client','clientservices','clientsupport','customers','customerservice','customersupport','customercare','ourcustomers','customer','clientservices','services','financialservices','professionalservices','managedservices','ourservices','productsservices','supportservices','consultingservices','howwehelpclients','productsandservices','cloudservices'],

    "investor_list_href" : ['investors','investorrelations','investor','investorcontacts','investorfaqs','investorinformation','investorcontact','investorcentre','investoroverview','investorscontact','ourinvestors','investorinfo','companyinvestors','partnersinvestors','executiveteam','executivemanagement','executiveleadership','executives','executivecommittee','executivebriefingcenter','executivebios','executivebriefingcenters','ourexecutiveteam','executiveboard','executivesummary','executiveofficers','executive','executivemanagementteam','customerserviceexecutive','businessdevelopmentexecutive']

}