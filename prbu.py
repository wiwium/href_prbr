import pymongo

try:
    # one out dir
    from href_prbu import config
    from href_prbu.src import clean_href,clean_urls,parse_href
except:
    # in own dir
    import config
    from src import clean_href,clean_urls,parse_href
    
connection = pymongo.MongoClient(config.PATHS['Mongo_ip'], config.PATHS['Mongo_port'])
collections = config.PATHS['out_db']
db = connection[config.PATHS['mongo_database']]


def run(cap=None):
    
    parse_href.ParseHref(db[collections['raw']],config.PATHS['ZIP'],cap=cap)
    cln_urls = clean_urls.CleanUrls(config.PATHS['URLS_CSV_PATH']).get_valid_urls()
    clean_href.CleanHref(cln_urls,db[collections['raw']],db[collections['clean']],db[collections['tel']],db[collections['mail']])
    
    
    for bt in config.bucket:
        find(config.bucket[bt],bt)
    

def find(bucket_list,output_db_name,db_clean_href=db[collections['clean']]):
    
    db_clean_href.aggregate(
        [
            { "$match": 
             { "$or":[
                 { "key_href": 
                  { "$in":
                    bucket_list
                  }
                 },
                 {"key_tag":
                  {"$in":
                   bucket_list
                    } 
                 }]
            }
         }, { "$out": output_db_name } ]);
    print(output_db_name)
    
    
    
#     for i in get_keys():
#         print(i)
def get_keys(db_clean_href=db[collections['clean']],top=10,tag=False):
    
    key_col = 'key_href'
    if tag:
        key_col = 'key_tag'
    
    return db_clean_href.aggregate([
        {"$group" : {"_id" : "$%s"%(key_col), "count" : {"$sum" : 1}}},
        {"$sort" : {"count" : -1}},
        {"$limit" : top}
    ])

if __name__ == "__main__":
    run()
    

# mongoexport --db ketan --collection discover_emails --type=csv --fields company_id,link --out "do_mails.csv"