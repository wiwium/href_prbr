import validators
import numpy as np
import pandas as pd
import csv
import re
import time

char_regEx = re.compile('[^a-z]')

class CleanHref:
    '''
            -- CleanHref(db_hrefs,db_clean_href,db_tel,db_mail)
                urls : cleaned urls returned by CleanUrls
                db_href : ParseHref returned mogodb collection
                db_clean_href : ouput cleaned href
                db_tel : ouput tel href
                db_mail : output mail href
                chunk_size : pandas df processing chunk for memory error
            starts exicution

    '''

    def get_next_chunk(self,chunk_size):
        for i in range(self.chunks+1):
            print('%s/%s'%(i,self.chunks+1))
            yield pd.DataFrame(list(self.db_href.find()[(i)*chunk_size:(i+1)*chunk_size]))
    
    def __init__(self,urls,db_href,db_clean_href,db_tel,db_mail,chunk_size=100000):
        
        hrefs_count = db_href.count()
        if hrefs_count == 0:
            raise Exception('no href found please check if you have at least one raw href in mongodb')
        print('hrefs to be cleaned : %s'%hrefs_count)
        self.chunks = int(hrefs_count/chunk_size)
        self.db_href = db_href
        start = time.time()
        print('started cleaning')
        
        for href in self.get_next_chunk(chunk_size):
            href.drop(href.columns[[0]], axis=1,inplace=True)
            clnhref = _CleanHref(href,urls)
            
            db_clean_href.insert_many(clnhref.get_join_href().to_dict('records'))
            db_mail.insert_many(clnhref.get_email_href().to_dict('records'))
            db_tel.insert_many(clnhref.get_tel_href().to_dict('records'))
            
        print('done cleaning href')
        print(time.time() - start)
        

class _CleanHref:
        
    def __init__(self,href, urls):

        self.urls = urls
           
        if not ('company_id' and 'company_name' and 'homepage_url') in self.urls.columns:
            raise Exception("urls columns are not matched should have ['company_id', 'homepage_url', 'company_name']")
        
        self.href = href
        
        if list(self.href) != ['company_id', 'fullTag', 'link']:
            raise Exception("href colums are not matched should be ['company_id', 'fullTag', 'link']")
            
            
        self.preprocess_hrefs()
        self.href_join = pd.merge(self.href,self.urls,how='inner',on=['company_id'])
        self.href = None # free href memory and let garbage collected
        self.href_join = self.href_join.apply(self.join_url,axis=1)
        ## Remove duplicates keep which has fullTag max
        self.href_join = self.href_join.sort_values('fullTag', ascending=False).drop_duplicates('link').sort_index()
        
        self.href_join['key_href'] = self.href_join['link'].apply(self.process_href_bucket)
        self.href_join['key_tag'] = self.href_join['fullTag'].apply(self.process_tag_bucket)
        
        
    @staticmethod  
    def process_href_bucket(link):
        if link is None:
            return ""
        link = str(link)
        if type(link) is not str:
            return ""
        link = link.rsplit('/', 1)[-1]
        link = link.rsplit('.', 1)[0]
        link = link.lower()
        link = char_regEx.sub("", link)
        return link

    @staticmethod
    def process_tag_bucket(tag):
        if tag is None:
            return ""
        tag = str(tag)
        if type(tag) is not str:
            return ""
        tag = tag.lower()
        tag = char_regEx.sub("", tag)
        return tag

        
    @staticmethod
    def join_url(row):
        if row['link'].startswith('/'):
            row['link'] = row['homepage_url'] + row['link'] 
        elif not row['link'].startswith('http'):
            row['link'] = row['homepage_url'] + '/' +  row['link']
        return row
    
        
    def preprocess_hrefs(self):
        
        # preprocess fullTag
        self.href['fullTag'] = self.href['fullTag'].str.strip()
        self.href['fullTag'] = self.href['fullTag'].str.replace('\n', ' ')
        self.href['fullTag'] = self.href['fullTag'].str.replace('\t', ' ')
        self.href['fullTag'] = self.href['fullTag'].fillna("")
        
        # preprocess links
        
        self.href['link'] = self.href['link'].str.strip()
        self.href = self.href[~self.href['link'].isnull()]
        self.href = self.href[~self.href.link.str.startswith('#')]
        self.href = self.href[~(self.href['link'] == '/')]
        self.href = self.href[~self.href.link.str.startswith('javascript:')]
        self.email_href = self.href[self.href.link.str.startswith('mailto:')]
        self.href = self.href[~self.href.link.str.startswith('mailto:')]
        self.tel_href = self.href[self.href.link.str.startswith('tel:')]
        self.href = self.href[~self.href.link.str.startswith('tel:')]
        
        self.href['link'] = self.href['link'].apply(self.clean_href)
        
        # private protocalls removed like whatsapp:// etc
        self.href = self.href[~(~(self.href['link'].str.startswith('http') | self.href['link'].str.startswith('/')) & (self.href['link'].str.contains('www') | self.href['link'].str.contains('http')))]
    
    
    @staticmethod
    def clean_href(link):
        
        if link == None:
            return ""
        if type(link) is not str:
            return ""
        link = link.split('#')[0]
        if link.startswith('//www.'):
            link = link.replace('//www.','http://www.',1)
        elif link.startswith('//http'):
            link = link.replace('//http','http',1)
        elif link.startswith('www.'):
            link = link.replace('www.','http://www.',1)
        elif link.startswith('Http'):
            link = link.replace('Http','http',1)
        elif link.startswith('//'):
            link = link.replace('//','http://www.',1)
        if link.startswith('internal://'):
            link = link.replace('internal://','http://',1)
        if link.startswith('\\"http'):
            link = link.replace('\\"http','http',1)
            if link.endswith('\\"'):
                link = link[:-2]
        if link.startswith(', http'):
            link = link.replace(', http','http',1)
        while link.endswith('/'):
            link = link[:-1]
        return link
     
    

    def get_email_href(self):
        mail = pd.merge(self.email_href,self.urls,how='inner',on=['company_id'])
        mail['link'] = mail['link'][mail['link'].str.startswith('mailto:')].apply(lambda x:x[7:].strip())
        mail = mail[~((mail['link'] == '') | (mail['link'] == '#'))]
        mail = mail[~(mail['link'].str.contains('^[^a-zA-Z]*$|^tel:|^[^a-zA-Z0-9]'))]
        return mail

    def get_tel_href(self):
        return pd.merge(self.tel_href,self.urls,how='inner',on=['company_id'])

    def get_join_href(self):
        return self.href_join
       