import numpy as np
import pandas as pd
import csv
import re

class CleanUrls:
    
    '''
        -- CleanUrls(urls)
            urls : raw urls
            
        
        concerned functions:
            -- get_valid_urls()
    '''
    
    def __init__(self,urls_path):

        print('loading urls csv file...')
        self.urls = pd.read_csv(urls_path)
        print('cleaning urls...')
        
        if list(self.urls) == ['uuid', 'name', 'website']:
            self.urls.rename(columns={'uuid': 'company_id','name':'company_name','website':'homepage_url'},inplace=True)
            
        if list(self.urls) == ['uuid', 'homepage_url', 'company_name', 'country_code']:
            self.urls.rename(columns={'uuid': 'company_id'},inplace=True)
            
        if not ('company_id' and 'company_name' and 'homepage_url') in self.urls.columns:
            raise Exception("urls columns are not matched should have ['company_id', 'homepage_url', 'company_name']")
        
        self.preprocess_urls()
        print('done cleaning urls')
        
        
    def preprocess_urls(self):
        
        self.urls = self.urls[~self.urls['homepage_url'].isnull()]
        self.urls['homepage_url'] = self.urls['homepage_url'].str.strip()
        self.urls['homepage_url'] = self.urls['homepage_url'].apply(self.clean_url)
        
        self.urls = self.urls[self.urls['homepage_url'].apply(lambda homepage_url: '.' in homepage_url)]
        
        
    
    @staticmethod
    def clean_url(homepage_url):
        
        # None shoudn't happen as in cleaned href null values are removed
        if homepage_url == None:
            return None
        
        try:
            if homepage_url.startswith('Http'):
                homepage_url = homepage_url.replace('Http','http',1)
            if homepage_url.startswith('https://.'):
                homepage_url = homepage_url.replace('https://.','https://',1)
            if homepage_url.startswith('http://.'):
                homepage_url = homepage_url.replace('http://.','http://',1)
            if homepage_url.endswith('.'):
                homepage_url = homepage_url[:-1]
            if homepage_url.endswith('./'):
                homepage_url = homepage_url[:-2]
            if homepage_url.endswith('//'):
                homepage_url = homepage_url[:-2]
            if homepage_url.endswith('/'):
                homepage_url = homepage_url[:-1]
            return homepage_url
        except:
            raise Exception("homepage_url should be string or None only")

    def get_valid_urls(self):
        return self.urls
