import pymongo
import numpy as np
import pandas as pd
from selectolax.parser import HTMLParser
import os
import time
from multiprocessing.dummy import Pool  # This is a thread-based Pool
from multiprocessing import cpu_count
import zipfile
from bs4 import BeautifulSoup
import warnings


class ParseHref:
    '''
        Init signature: ParseHref(db_collection, zip_path)
        
        Parameters
        ----------
        db_collectio : pymongo mongodb collection in which hrefs to be stored
        
        zip_path : zip file path which contains folders(name as keys) which contains htmls 
        
    '''

    
    def __init__(self, db_collection, zip_path,fast=False,cap=None):
        start = time.time()
        self.fast = fast
        
        if self.fast:
            warnings.warn('\nSetting "fast=True" may cause segmentaion fault due to call stack is getting full https://github.com/rushter/selectolax/issues/5  \nYou can increase call stack limit by \n "ulimit -s unlimited" if not woking try\n \'sudo sh -c "ulimit -n 65535 && exec su $LOGNAME"\'\n')
        
        print('finding hrefs...')
        self.db_collection = db_collection
        self.html_zip = zipfile.ZipFile(zip_path)
        self.html_list = [d for d in self.html_zip.namelist() if d.endswith(".html")]
        print('total %s files'%(len(self.html_list)))
        self.main(cap)
        print(time.time() - start)

    
    def bs(self,content,company_id):
        soup = BeautifulSoup(content, "lxml")
        for hrefTag in soup.findAll('a', href=True):
            href = hrefTag['href']
            self.db_collection.insert({'company_id': company_id, 'link': href, 'fullTag': hrefTag.text})
    
    def solax(self,content,company_id):
        soup = HTMLParser(content)
        for tag in soup.tags('a'):
            attrs = tag.attributes
            if 'href' in attrs:
                href = attrs['href']
                self.db_collection.insert({'company_id': company_id, 'link': href, 'fullTag': tag.text()})
                
    
    def read_file(self, filename):
        company_id = filename.split(r'/')[-2]
        try:
            with self.html_zip.open(filename) as html_file:
                content = html_file.read()
            if self.fast:
                self.solax(content,company_id)
            else:
                self.bs(content,company_id)
        except:
            return

    def main(self,cap):

        pool = Pool(cpu_count() * 2)
        pool.map(self.read_file, self.html_list[:cap])

# arr = [j for i in results for j in i]
